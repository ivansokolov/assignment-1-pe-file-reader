/// @file
#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>

/// Suppress the "unused variable" warning
#define UNUSED(X) ((void) (X))

/// Disallow the compiler from inserting padding in a struct
#define PACKED __attribute__((packed))

/// Raise the alignment requirement of a type
#define ALIGNED(AMT) __attribute__((aligned(AMT)))

/// Mark a function that never returns
#define NORETURN _Noreturn

/// Mark a function whose arguments behave like a printf-style format
#define LIKE_PRINTF(NUM_ARGS) __attribute__((format(printf, NUM_ARGS, (NUM_ARGS)+1)))

/// Similar to `inline` in C++, in principle allows to define symbols in headers
#define HEADERDEF __attribute__((weak))

/// Influence the optimizer's idea of what's likely or possible
/// @{
#define LIKELY(X) __builtin_expect(!!(X), 1)
#define UNLIKELY(X) __builtin_expect(!!(X), 0)
#define ASSUME(X) do { if (!(X)) __builtin_unreachable(); } while (0)
/// @}

/// Static assertion
#define STATIC_ASSERT(EXPR,MSG) _Static_assert(EXPR, MSG)

/// Abort, printing a formatted message
NORETURN void LIKE_PRINTF(1) fatal (const char* fmt, ...);

/// Print a formatted warning message
void LIKE_PRINTF(1) info (const char* fmt, ...);


/// The clang-tidy config disallows the real versions of these functions for no good reason
/// @{
HEADERDEF void* memcpy_but_worse (void* dest, const void* src, size_t amount)
{
	char* cd = dest;
	const char* cs = src;
	while (amount--)
		*cd++ = *cs++;
	return dest;
}
#define memcpy(DEST,SRC,AMT) memcpy_but_worse(DEST,SRC,AMT)

HEADERDEF void* memset_but_worse (void* dest, int value, size_t amount)
{
	char* cd = dest;
	while (amount--)
		*cd++ = (char) value;
	return dest;
}
#define memset(DEST,VALUE,AMT) memset_but_worse(DEST,VALUE,AMT)
/// @}

/// Wrappers for allocation functions that ensure they succeed, or abort the program
/// @{
void* malloc_or_die (size_t size);
void* calloc_or_die (size_t nmemb, size_t size);
void* realloc_or_die (void* ptr, size_t size);
/// @}

#endif /* UTIL_H */
