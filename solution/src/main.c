/// @file
#include "pe.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>

/// Print program usage.
/// @param name Program name to display in the synopsis.
static void usage (const char* name)
{
  fprintf(stderr, "Usage: %s <in-file> <section-name> <out-file>\n", name);
}

/// Read PE sections from a file, or fail doing so.
/// @param filename Name of file to read from
/// @return The resulting PE sections object
static PE_sections pe_sections_from_file (const char* filename)
{
	FILE* in = fopen(filename, "r");
	if (!in)
		fatal("Failed to open input PE file \"%s\"", filename);

	PE_sections result;
	PE_read_result status = pe_sections_get(&result, in);

	switch (status) {
	case PE_READ_BAD_STREAM:
		fatal("Failed to read PE file \"%s\": bad input stream", filename);
	case PE_READ_BAD_HEADER:
		fatal("Failed to read PE file \"%s\": bad PE headers in file", filename);
	case PE_READ_OK: break;
	}

	fclose(in);
	return result;
}

/// Write PE section to file, or fail doing so.
/// @param filename Name of file to write to.
/// @param sect PE section. Not null.
static void pe_write_section (const char* filename, const PE_section* sect)
{
	FILE* out = fopen(filename, "wb");
	if (!out)
		fatal("Failed to open output file \"%s\"", filename);

	if (fwrite(sect->data, sect->size, 1, out) != 1)
		fatal("Failed to write to output file \"%s\"", filename);

	fclose(out);
}

/// Program entry point.
/// @param argc Number of program arguments
/// @param argv Argument vector
/// @return Zero on success, nonzero on failure
int main (int argc, char** argv)
{
	if (argc != 4) {
		usage(argv[0]);
		return 1;
	}

	const char* in_filename = argv[1];
	const char* section_name = argv[2];
	const char* out_filename = argv[3];

	PE_sections sections = pe_sections_from_file(in_filename);
	PE_section* sect = pe_find_section_by_name(&sections, section_name);
	pe_write_section(out_filename, sect);

	pe_sections_destroy(&sections);
  return 0;
}
