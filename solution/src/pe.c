/// @file
#include "pe.h"
#include "util.h"
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/// The offset of a 4-byte offset into the while at which the real PE header is found
#define PE_SIGNATURE_LOC_OFFS 0x3c

/// The PE magic value, as per docs
#define PE_MAGIC ('P' | ('E' << 8))

/// In-file representation of the PE header
typedef struct PE_header_packed {
	uint32_t magic;         ///< Must be PE_MAGIC
	uint16_t machine;
	uint16_t num_sections;  /// Number of sections
	uint32_t time_date_stamp;
	uint32_t symbol_table_ptr;
	uint32_t num_symbols;
	/// Size of optional header, used to determine the number of sections
	uint16_t sz_opt_header;
	uint16_t characteristics;
} PACKED ALIGNED(4) PE_header_packed;
STATIC_ASSERT(sizeof(PE_header_packed) == 24, "Bad PE header layout");

/// In-file representation of a PE section
typedef struct PE_section_header_packed {
	char name[PE_NAME_SIZE];  ///< Name, as per docs
	uint32_t virtual_size;
	uint32_t virtual_addr;
	uint32_t raw_data_size;   ///< The size of raw data elsewhere in the file
	uint32_t raw_data_ptr;    ///< The location of raw data elsewhere in the file
	uint32_t relocs_ptr;
	uint32_t linenr_ptr;
	uint16_t relocs_num;
	uint16_t linner_num;
	uint32_t characteristics;
} PACKED ALIGNED(4) PE_section_header_packed;
STATIC_ASSERT(sizeof(PE_section_header_packed) == 40, "Bad PE section layout");


PE_read_result pe_sections_get (PE_sections* pe, FILE* stream)
{
	/* Only used for returning errors, before which should be set to something else */
	PE_read_result status = PE_READ_OK;

	/* Can be repointed to a different cleanup label when more cleanup is required */
#define CLEANUP_LABEL cleanup_none

#define RETURN_ERROR(E) do { status = (E); goto CLEANUP_LABEL; } while (0)
#define TRY_READ(P,S,N) \
	do { \
		size_t n_ = (N); \
		if (fread(P, S, n_, stream) != n_) \
			RETURN_ERROR(PE_READ_BAD_STREAM); \
	} while (0)
#define TRY_SEEK(OFFS,MODE) \
	do { \
		if (fseek(stream, OFFS, MODE)) \
			RETURN_ERROR(PE_READ_BAD_STREAM); \
	} while (0)

	if (ftell(stream) != 0)
		RETURN_ERROR(PE_READ_BAD_STREAM);

	{
		TRY_SEEK(PE_SIGNATURE_LOC_OFFS, SEEK_SET);
		uint32_t offset;
		TRY_READ(&offset, sizeof(offset), 1);
		TRY_SEEK(offset, SEEK_SET);
	}

	PE_header_packed header;
	TRY_READ(&header, sizeof(header), 1);
	if (header.magic != PE_MAGIC)
		RETURN_ERROR(PE_READ_BAD_HEADER);

	TRY_SEEK(header.sz_opt_header, SEEK_CUR);
	int num_sections = header.num_sections;

	PE_section_header_packed* section_headers
		= malloc_or_die(sizeof(*section_headers) * num_sections);
	PE_section* sections = malloc_or_die(sizeof(*sections) * num_sections);

	int created_sections = 0; /* For cleanup in case of partial success */
#undef CLEANUP_LABEL
#define CLEANUP_LABEL cleanup_section_headers

	TRY_READ(section_headers, sizeof(PE_section_header_packed), num_sections);

	for (int i = 0; i < num_sections; i++) {
		PE_section_header_packed* src = section_headers + i;
		PE_section* dest = sections + i;

		STATIC_ASSERT(sizeof(dest->name) == sizeof(src->name), "Bad section name layout");
		memcpy(sections[i].name, section_headers[i].name, PE_NAME_SIZE);

		dest->data = malloc_or_die(src->raw_data_size);
		dest->size = src->raw_data_size;
		created_sections++;

		TRY_SEEK(src->raw_data_ptr, SEEK_SET);
		TRY_READ(dest->data, dest->size, 1);
	}

	free(section_headers);

	pe->sections = sections;
	pe->num_sections = num_sections;
	return PE_READ_OK;

cleanup_section_headers:
	for (int i = 0; i < created_sections; i++)
		free(sections[i].data);
	free(sections);
	free(section_headers);

cleanup_none:
	assert(status != PE_READ_OK);
	return status;
}

void pe_sections_destroy (PE_sections* pe)
{
	for (size_t i = 0; i < pe->num_sections; i++)
		free(pe->sections[i].data);
	free(pe->sections);

	pe->num_sections = 0;
	pe->sections = NULL;
}


PE_section* pe_find_section_by_name (const PE_sections* pe, const char* name)
{
	size_t len = strlen(name);

	if (len > PE_NAME_SIZE) /* No section name is ever longer than PE_NAME_SIZE */
		return NULL;

	char name_padded[PE_NAME_SIZE];
	memcpy(name_padded, name, len);
	memset(name_padded+len, 0, sizeof(name_padded)-len);

	for (size_t i = 0; i < pe->num_sections; i++) {
		PE_section* s = pe->sections + i;
		STATIC_ASSERT(sizeof(name_padded) == sizeof(s->name), "Bad section name layout");
		if (!memcmp(s->name, name_padded, sizeof(name_padded)))
			return s;
	}

	return NULL;
}
