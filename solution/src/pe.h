/// @file
#ifndef PE_H
#define PE_H

#include <stdbool.h>
#include <stdio.h>

/// As per PE docs
#define PE_NAME_SIZE 8

/// @brief A single section representing the data of a PE section. Owned and is managed by its
/// respective PE_sections object.
/// @sa https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#section-table-section-headers
/// @sa PE_sections
typedef struct {
	char name[PE_NAME_SIZE]; ///< Name, as per doc. 0-padded, but not necessarily 0-terminated
	void* data;              ///< Pointer to raw section data as taken from the PE file
	size_t size;             ///< Size of above raw data
} PE_section;

/// @brief The container for all sections of a single PE file.
/// Lifetime is managed with pe_sections_get(), pe_sections_destroy().
/// @sa PE_section
typedef struct {
	size_t num_sections;   ///< Number of sections, as taken from the PE file
	PE_section* sections;  ///< Pointer to the array of sections
} PE_sections;


typedef enum {
	PE_READ_OK,         ///< OK, object populated
	PE_READ_BAD_STREAM, ///< Bad input stream (failed to seek to or read from where expected)
	PE_READ_BAD_HEADER, ///< Bad values in file header (does not seem to be a valid PE file)
} PE_read_result;
/// @brief Read a PE file and populate a PE_sections object.
/// @sa pe_sections_destroy
/// @param pe Sections to populate. Not null. Must not already be populated with data,
///           else leaks will occur.
/// @param stream The stream with the PE file open. Because the function relies on
///               absolute file offsets, the current cursor must be at the beginning.
/// @return The result enum. If the result is not PE_READ_OK, pe is not written to.
PE_read_result pe_sections_get (PE_sections* pe, FILE* stream);

/// End the lifetime of a PE sections object, cleaning up its data
/// @sa pe_sections_get
/// @param pe Sections to clean up. Not null. Must have been populated with
///           a previous call to pe_sections_get
void pe_sections_destroy (PE_sections* pe);


/// Find a section with a given name from a PE sections object, correctly comparing the
/// names.
/// @param pe Sections to seek from. Not null. Must have been populated with a previous
///           pe_sections_get() call.
/// @param name C-string with the name of the section. Not null.
/// @return Pointer to one of pe's `sections`, or null if not found
PE_section* pe_find_section_by_name (const PE_sections* pe, const char* name);

#endif /* PE_H */
