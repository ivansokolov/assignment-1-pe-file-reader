#!/bin/sh
rm -r build-*
for t in Release Debug ASan LSan UBSan; do
	echo "================= $t ================="
	cmake -B "build-$t" "-DCMAKE_BUILD_TYPE=$t"
	cmake --build "build-$t"
	cd "build-$t"
	ctest .
	cd ..
done
